# BearSH

## Install

#### NOTE: make sure you have go lang and make installed before you try to install bearsh!

To install, run:

```shell
git clone https://gitlab.com/unic0rn9k/bearsh.git
cd bearsh
make install
```

If you install bearsh as a wrapper for zsh, it will replace your old zshrc. If you want to add bearsh to an existing zsh config, run `bearsh zsh >> ~/.zshrc`. Or you can  run `make update` instead of make install when installing bearsh (this will install bearsh, but will not replace ~/.zshrc).

To update bearsh, simply run: `bearsh update`

## Having trouble?

​	If `make install` doesn't work, then try `make default`

​	if `bearsh update` doesn't work try `bearsh updateAuto`

## How to use

You can run bearsh with the command `bearsh`, however i would recommend installing zsh and installing it as a wrapper for zsh. When you have bearsh for zsh installed, you should run `zsh` instead of bearsh. To install rice `bearsh getrice` for bearsh. a *"rice"* is a set of configurations that includes a theme, plugins and such.

## Updates: bjorn (v0.7.2)

#### Bearsh now works as a replacement for oh-my-zsh, and its literally a tenth of the size!

### New functionality:

Uninstall bearsh by running `bearsh uninstall`.

Watch your todo list by running `todo ls` and get a help menu for "todo" by running `todo help`. (use remcas -nw org mode instead)

Run `please` instead of `sudo` for a more wholesome terminal experience.

If you have "pass" installed you can run `fpass` to search trough your passwords.

Run `bearsh getrice` to view and install rice/themes.

### Changes:

- themeing support

- plugin manager
- added ~/.bearsh as location for bearsh dotfiles and plugins
- built in todo manager
- removed oh-my-zsh as dependency

- removed logrusorgru/aurora as dependency
- added fzf as dependency
- added zsh-command-time as dependency

- improved install process
- added rice package manager
