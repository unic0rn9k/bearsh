for plugin ($plugins); do
    source $ZSH/$plugin/*.zsh
done

export precmd() {
export error="$?"
export jobs="$(jobs -r -p)"
PROMPT=$(bearsh status)
#RPROMPT=%{${_lineup}%}"$(date)"%{${_linedown}%}
RPROMPT=""
}

function zsh_array_as_bearsh_array { local IFS="$1"; shift; echo "$*"; }

alias -g ...='../..'
alias -g ....='../../..'
alias -g .....='../../../..'
alias -g ......='../../../../..'

alias ls='ls --color=auto -hF'
alias grep='grep --color=auto'
alias please='sudo'
alias _='sudo'
alias rn='ranger'
alias nvi='nvim'
alias fpass='pass $(cd $HOME/.password-store;fzf | sed "s/\..*//")'
alias mcnew='sudo sh -c "ifconfig wlp63s0 down;macchanger -r wlp63s0;ifconfig wlp63s0 up"'
alias C='code ./'
alias delete='rm -rfv $(fzf -m)'
alias mold='fzf -m | while read id;do mv "$id" "${id}.old" -fv;done'

export NIMPATH="$HOME/.nimpath"
alias cnimp='cp -rfv $(fzf -m) $NIMPATH'
alias nimc='nim c --path=$NIMPATH'

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export FZF_DEFAULT_OPS="--extended"
export FZF_DEFAULT_COMMAND="fd --type f"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

zstyle ':completion:*' menu select
zstyle ':completion:*' completer _complete
zstyle ':completion:*' matcher-list '' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' '+l:|=* r:|=*'

autoload -U compinit && compinit
zmodload -i zsh/complist

unsetopt menu_complete   # do not autoselect the first completion entry
unsetopt flowcontrol
setopt auto_menu         # show completion menu on successive tab press
setopt complete_in_word
setopt always_to_end

unset CASE_SENSITIVE HYPHEN_INSENSITIVE

zstyle ':completion:*' special-dirs true

zstyle ':completion:*' list-colors ''
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01'

# disable named-directories autocompletion
zstyle ':completion:*:cd:*' tag-order local-directories directory-stack path-directories

# Use caching so that commands like apt and dpkg complete are useable
zstyle ':completion::complete:*' use-cache 1
zstyle ':completion::complete:*' cache-path $ZSH/cache

## History command configuration
[ -z "$HISTFILE" ] && HISTFILE="$HOME/.zsh_history"
HISTSIZE=50000
SAVEHIST=10000

setopt extended_history       # record timestamp of command in HISTFILE
setopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_ignore_dups       # ignore duplicated commands history list
setopt hist_ignore_space      # ignore commands that start with space
setopt hist_verify            # show command with history expansion to user before running it
setopt inc_append_history     # add commands to HISTFILE in order of execution
setopt share_history          # share command history data

if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
  function zle-line-init() {
    echoti smkx
  }
  function zle-line-finish() {
    echoti rmkx
  }
  zle -N zle-line-init
  zle -N zle-line-finish
fi

# start typing + [Up-Arrow] - fuzzy find history forward
if [[ "${terminfo[kcuu1]}" != "" ]]; then
  autoload -U up-line-or-beginning-search
  zle -N up-line-or-beginning-search
  bindkey "${terminfo[kcuu1]}" up-line-or-beginning-search
fi
# start typing + [Down-Arrow] - fuzzy find history backward
if [[ "${terminfo[kcud1]}" != "" ]]; then
  autoload -U down-line-or-beginning-search
  zle -N down-line-or-beginning-search
  bindkey "${terminfo[kcud1]}" down-line-or-beginning-search
fi

bindkey ' ' magic-space                               # [Space] - do history expansion

bindkey '^[[1;5C' forward-word                        # [Ctrl-RightArrow] - move forward one word
bindkey '^[[1;5D' backward-word                       # [Ctrl-LeftArrow] - move backward one word

if [[ "${terminfo[kcbt]}" != "" ]]; then
  bindkey "${terminfo[kcbt]}" reverse-menu-complete   # [Shift-Tab] - move through the completion menu backwards
fi

bindkey '^?' backward-delete-char                     # [Backspace] - delete backward
if [[ "${terminfo[kdch1]}" != "" ]]; then
  bindkey "${terminfo[kdch1]}" delete-char            # [Delete] - delete forward
else
  bindkey "^[[3~" delete-char
  bindkey "^[3;5~" delete-char
  bindkey "\e[3~" delete-char
fi

function clipcopy() {
  emulate -L zsh
  local file=$1
  if [[ $OSTYPE == darwin* ]]; then
    if [[ -z $file ]]; then
      pbcopy
    else
      cat $file | pbcopy
    fi
  elif [[ $OSTYPE == cygwin* ]]; then
    if [[ -z $file ]]; then
      cat > /dev/clipboard
    else
      cat $file > /dev/clipboard
    fi
  else
    if (( $+commands[xclip] )); then
      if [[ -z $file ]]; then
        xclip -in -selection clipboard
      else
        xclip -in -selection clipboard $file
      fi
    elif (( $+commands[xsel] )); then
      if [[ -z $file ]]; then
        xsel --clipboard --input 
      else
        cat "$file" | xsel --clipboard --input
      fi
    else
      print "clipcopy: Platform $OSTYPE not supported or xclip/xsel not installed" >&2
      return 1
    fi
  fi
}

function clippaste() {
  emulate -L zsh
  if [[ $OSTYPE == darwin* ]]; then
    pbpaste
  elif [[ $OSTYPE == cygwin* ]]; then
    cat /dev/clipboard
  else
    if (( $+commands[xclip] )); then
      xclip -out -selection clipboard
    elif (( $+commands[xsel] )); then
      xsel --clipboard --output
    else
      print "clipcopy: Platform $OSTYPE not supported or xclip/xsel not installed" >&2
      return 1
    fi
  fi
}

function todo {
  if [[ "x$1" == "xedit" ]];
  then
    $EDITOR $HOME/.bearsh/notes/$(cd $HOME/.bearsh/notes;fzf)
  elif [[ "x$1" == "xset" ]];
  then
    tomove=$(cd $HOME/.bearsh/notes;ls | while read id;do echo "$id: $(cat $id)";done | fzf | sed "s/\:.*//")
    echo "$tomove: "$(cat "$HOME/.bearsh/notes/$tomove") >> $HOME/.bearsh/notes/DONE
    rm -f "$HOME/.bearsh/notes/$tomove"
    todo ls
  elif [[ "x$1" == "xls" ]];
  then
    ls $HOME/.bearsh/notes | while read id;do echo "`tput bold`$id:`tput sgr0` $(cat $HOME/.bearsh/notes/$id)";done
  elif [[ "x$1" == "xclean" ]];
  then
    rm -fv $HOME/.bearsh/notes/DONE
    todo ls
  elif [[ "x$1" == "xhelp" ]];
  then
    echo "bearsh -> todo -> help
    the todo files are placed in '~/.bearsh/notes'
    todo <option>
    edit: choose and edit a todo file
    set: mark a todo file as done
    ls: list todo files
    clean: remove all todo files marked as done
    help: show this help message
    <file to edit>: edit a new or existing todo file"
  else
    $EDITOR "$HOME/.bearsh/notes/$1"
  fi
}

zsh_command_time() {
    if [ -n "$ZSH_COMMAND_TIME" ]; then
        hours=$(($ZSH_COMMAND_TIME/3600))
        min=$(($ZSH_COMMAND_TIME/60))
        sec=$(($ZSH_COMMAND_TIME%60))
        if [ "$ZSH_COMMAND_TIME" -le 60 ]; then
            timer_show="${ZSH_COMMAND_TIME}s"
        elif [ "$ZSH_COMMAND_TIME" -gt 60 ] && [ "$ZSH_COMMAND_TIME" -le 180 ]; then
            timer_show="${min}min ${sec}s"
        else
            if [ "$hours" -gt 0 ]; then
                min=$(($min%60))
                timer_show="${hours}h ${min}min ${sec}s"
            else
                timer_show="${min}min ${sec}s"
            fi
        fi
        #right_bear=$(bearsh status "theme:took\s${timer_show}:color=yellow:type=text")
        #RPROMPT="${right_bear}"
        local _lineup=$'\e[1A'
        local _linedown=$'\e[1B'
        RPROMPT="%F{$ZSH_COMMAND_TIME_COLOR}took $timer_show%f"
    fi
}

ZSH_COMMAND_TIME_MIN_SECONDS=2
ZSH_COMMAND_TIME_COLOR="yellow"
