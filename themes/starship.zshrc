# this file came from bearsh. the old ".zshrc" file can be found in "~/.zshrc.old". to add bearsh to an existing zsh config, run: bearsh zsh >> .zshrc
#NOTE : this theme/rice requires starship!
export ZSH="$HOME/.bearsh"

plugins=(
	zsh-syntax-highlighting
	zsh-autosuggestions
)

source $ZSH/bear.zsh
export PATH="$HOME/cargo/bin:$PATH"
source <("/usr/local/bin/starship" init zsh --print-full-init)
