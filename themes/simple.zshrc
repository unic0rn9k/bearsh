# this file came from bearsh. the old ".zshrc" file can be found in "~/.zshrc.old". to add bearsh to an existing zsh config, run: bearsh zsh >> .zshrc
export ZSH="$HOME/.bearsh"

plugins=(
	zsh-syntax-highlighting
	zsh-autosuggestions
	zsh-command-time
)

# VVVV BearSH theme VVVV
export bearsh_theme="
userInfo:root=bold,red
 pwd:locked_path_color=red:path_color=cyan
 gitInfo:branch_color=reset,yellow:change_color=reset,red,bold
 jobInfo:color=yellow
 prompt:char=\n>
 segments:type=ascii
"
#^^^^ BearSH theme ^^^^

source $ZSH/bear.zsh