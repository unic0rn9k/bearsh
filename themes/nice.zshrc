# this file came from bearsh. the old ".zshrc" file can be found in "~/.zshrc.old". to add bearsh to an existing zsh config, run: bearsh zsh >> .zshrc
#bearsh git : https://github.com/popstas/zsh-command-time.git : zsh-command-time
#NOTE : this theme/rice requires nerdfont!
export ZSH="$HOME/.bearsh"

plugins=(
	zsh-syntax-highlighting
	zsh-autosuggestions
	zsh-command-time
)

# VVVV BearSH theme VVVV
export bearsh_theme="
userInfo:root=bold,red
 pwd:locked_path_color=red,bold:path_color=cyan,bold
 gitInfo:branch_color=dark_gray:change_color=yellow:branch_icon_color=dark_gray:branch_change_color=red
 jobInfo:color=yellow
 prompt:char=\n●
 segments:type=round
"
# ^^^^ BearSH theme ^^^^

export EDITOR='nvim' # <- remember to set your text editor ;)

source $ZSH/bear.zsh

todo ls