# this file came from bearsh. the old ".zshrc" file can be found in "~/.zshrc.old". to add bearsh to an existing zsh config, run: bearsh zsh >> .zshrc
export ZSH="$HOME/.oh-my-zsh"
plugins=(
	git
	zsh-syntax-highlighting
	zsh-autosuggestions
	zsh-command-time
)
source $ZSH/oh-my-zsh.sh

# VVVV BearSH theme VVVV
export bearsh_theme="
userInfo:root=bold,red
 pwd:locked_path_color=red:path_color=cyan
 gitInfo:branch_color=yellow:change_color=red
 projectsInPath:lang_color=green:sys_color=magenta
 jobInfo:color=yellow
 prompt:char=\n->
 segments:type=powerline
"
# ^^^^ BearSH theme ^^^^

export precmd() {
PROMPT=$(bearsh status "prompt:$?;theme:$bearsh_theme;jobs:$(jobs -r -p)")
RPROMPT=""
}

alias ls='ls --color=auto -hF'
alias grep='grep --color=auto'
alias please='sudo'
alias _='sudo'
alias rn='ranger'
alias nvi='nvim'
alias fpass='pass $(cd $HOME/.password-store;fzf | sed "s/\..*//")'
alias mcnew='sudo sh -c "ifconfig wlp63s0 down;macchanger -r wlp63s0;ifconfig wlp63s0 up"'
alias C='code ./'

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

zsh_command_time() {
    if [ -n "$ZSH_COMMAND_TIME" ]; then
        hours=$(($ZSH_COMMAND_TIME/3600))
        min=$(($ZSH_COMMAND_TIME/60))
        sec=$(($ZSH_COMMAND_TIME%60))
        if [ "$ZSH_COMMAND_TIME" -le 60 ]; then
            timer_show="${ZSH_COMMAND_TIME}s"
        elif [ "$ZSH_COMMAND_TIME" -gt 60 ] && [ "$ZSH_COMMAND_TIME" -le 180 ]; then
            timer_show="${min}min\s${sec}s"
        else
            if [ "$hours" -gt 0 ]; then
                min=$(($min%60))
                timer_show="${hours}h\s${min}min\s${sec}s"
            else
                timer_show="${min}min\s${sec}s"
            fi
        fi
        #right_bear=$(bearsh status "theme:took\s${timer_show}:color=yellow:type=text")
        #RPROMPT="${right_bear}"
        local _lineup=$'\e[1A'
        local _linedown=$'\e[1B'
        RPROMPT="%F{$ZSH_COMMAND_TIME_COLOR}took $timer_show%f"
    fi
}

ZSH_COMMAND_TIME_MIN_SECONDS=2
ZSH_COMMAND_TIME_COLOR="yellow"