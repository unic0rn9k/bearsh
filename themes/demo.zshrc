# this file came from bearsh. the old ".zshrc" file can be found in "~/.zshrc.old". to add bearsh to an existing zsh config, run: bearsh zsh >> .zshrc
export ZSH="$HOME/.bearsh"

plugins=(
	zsh-syntax-highlighting
	zsh-autosuggestions
	zsh-command-time
)

# VVVV BearSH theme VVVV
export bearsh_theme="
${HOST}:type=text:color=red,bold
 userInfo:root=bold,red
 pwd:locked_path_color=red:path_color=cyan
 gitInfo:branch_color=yellow:change_color=red
 projectsInPath:lang_color=green:sys_color=magenta
 jobInfo:color=yellow
 prompt:char=\n->
 segments:type=powerline
"
# ^^^^ BearSH theme ^^^^

export EDITOR='nvim' # <- remember to set your text editor ;)

source $ZSH/bear.zsh

todo ls