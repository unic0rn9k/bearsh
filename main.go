package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strings"
)

const version = "v0.7.3 bjorn"

/*
  ____                  _____ __   __
 |  _ \                / ____| |  | |
 | |_) | ___  __ _ _ _| (___ | |__| |
 |  _ ( / _ \/ _` | '__\___ \|  __  |
 | |_) |  __| (_| | |  ____) | |  | |
 |____/ \___|\__,_|_| (_____/|_|  |_|

 BearSH - version: 0.7.3 bjorn

*/

var fmtMap = map[string]string{
	"red":       "\x1b[31m",
	"green":     "\x1b[32m",
	"yellow":    "\x1b[33m",
	"blue":      "\x1b[34m",
	"magenta":   "\x1b[35m",
	"cyan":      "\x1b[36m",
	"dark_gray": "\x1b[90m",

	"bg_red":       "\x1b[41m",
	"bg_green":     "\x1b[42m",
	"bg_yellow":    "\x1b[43m",
	"bg_blue":      "\x1b[44m",
	"bg_magenta":   "\x1b[45m",
	"bg_cyan":      "\x1b[46m",
	"bg_dark_gray": "\x1b[100m",

	"bold":     "\x1b[1m",
	"reset":    "\x1b[0m",
	"inverted": "\x1b[7m",
	"dim":      "\x1b[2m",
	"blink":    "\x1b[5m",
}

var promptChar string
var theme map[string]map[string]string

func addFmt(color string, in ...interface{}) string {
	var out string
	for _, efx := range strings.Split(color, ",") {
		out += fmtMap[efx]
	}
	return out + fmt.Sprint(in...) + fmtMap["reset"]
}

func xCmd(in string) error {
	cmd := exec.Command("sh", "-c", in)
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
	return cmd.Run()
}

func sxCmd(in string) (string, error) {
	out, err := exec.Command("sh", "-c", in).Output()
	return string(out), err
}

func sCmd(in string) string {
	out, _ := exec.Command("sh", "-c", in).Output()
	return string(out)
}

var segmentChars = map[string][]string{
	"round":     {"", "", "", "", ""},
	"ascii":     {"", "[", "]", " |", " |"},
	"":          {"", "", "", "", ""},
	"powerline": {"inverted", "", "", "", ""},
	"fire":      {"inverted", "\uE0C0  ", "\uE0C0  ", "\uE0C0  ", "\uE0C0  "},
}

var segmentType = "ascii"

func segmentStart(color string) string {
	return addFmt(segmentChars[segmentType][0], addFmt(color, segmentChars[segmentType][1]))
}

func segmentEnd(color string) string {
	return addFmt(color, segmentChars[segmentType][2])
}

func segment(colorA, colorB string, in string) string {
	return addFmt("inverted,"+colorA, in) + addFmt(colorB+","+colorA, segmentChars[segmentType][3])
}

func segmentFade(colorA, colorB string) string {
	return addFmt("inverted", addFmt(colorB, addFmt(colorA, segmentChars[segmentType][4])))
}

func segmentArray(colors []string, data []string) string {
	if len(colors) == len(data) {
		var out string
		out += segmentStart(colors[0])
		for n := 0; n < len(data)-1; n++ {
			out += addFmt("inverted,bold,"+colors[n], " "+data[n]+" ") + segmentFade("bg_"+colors[n], colors[n+1])
		}
		out += addFmt("inverted,bold,"+colors[len(colors)-1], " "+data[len(colors)-1]+" ") + segmentEnd(colors[len(colors)-1])
		return out
	}
	return ""
}

func statusElement(elementName string) string {
	var elementToLoadString string
	switch elementName {

	case "pwd":
		// path
		pwd, _ := os.Getwd()
		dirPerm := sCmd("ls -ld \"" + pwd + "\"")
		dirPerm = strings.Split(dirPerm, " ")[2]
		pwd = strings.Replace(pwd, os.Getenv("HOME"), "~", -1)
		if pwd == "~" {
			pwd = ""
		}
		if len(pwd) > 20 {
			pwd2 := ".../" + strings.Split(pwd, "/")[len(strings.Split(pwd, "/"))-1]
			if len(pwd2) < len(pwd) {
				pwd = pwd2
			}
		}
		if dirPerm != os.Getenv("USER") && os.Getenv("USER") != "root" {
			pwd = addFmt(theme["pwd"]["locked_path_color"], pwd+" ")
		} else {
			pwd = addFmt(theme["pwd"]["path_color"], pwd+" ")
		}
		elementToLoadString = pwd
		break

	case "userInfo":
		// user info
		if hostname := strings.Trim(sCmd("hostname"), "\n"); os.Getenv("USER") != hostname {
			elementToLoadString = os.Getenv("USER") + " @ " + hostname + " -> "
		}
		if os.Getenv("USER") == "root" {
			elementToLoadString = addFmt("inverted,"+theme["userInfo"]["root"], " ROOT ") + segmentEnd(theme["userInfo"]["root"]) + " "
		}
		break

	case "gitInfo":
		//git
		gitChanges := len(strings.Split(sCmd("git status -s"), "\n")) - 1
		if _, err := sxCmd("git rev-parse --abbrev-ref HEAD"); err == nil {
			if gitChanges != 0 {

				var gitChangesStr string
				gitChangesMap := make(map[string]int)
				for _, change := range strings.Split(strings.Trim(sCmd("git status -s"), "\n"), "\n") {
					gitChangesMap[strings.Replace(string(change[0])+string(change[1]), " ", "", -1)]++
				}
				for n := range gitChangesMap {
					gitChangesStr += fmt.Sprint(gitChangesMap[n], n, " ")
				}
				if theme["gitInfo"]["branch_change_color"] != "" {
					theme["gitInfo"]["branch_color"] = theme["gitInfo"]["branch_change_color"]
				}
				elementToLoadString = segmentStart(theme["gitInfo"]["branch_color"]) + segment(theme["gitInfo"]["branch_color"], "bg_"+theme["gitInfo"]["change_color"], addFmt("bold", "  "+strings.Trim(sCmd("git rev-parse --abbrev-ref HEAD"), "\n")))
				elementToLoadString += addFmt("inverted,bold,"+theme["gitInfo"]["change_color"], " "+gitChangesStr)
				elementToLoadString += segmentEnd(theme["gitInfo"]["change_color"])
			} else {
				elementToLoadString = addFmt(theme["gitInfo"]["branch_icon_color"], " ") + addFmt(theme["gitInfo"]["branch_color"], strings.Trim(sCmd("git rev-parse --abbrev-ref HEAD"), "\n"))
			}
		}
		break

	case "jobInfo":
		//jobs
		if os.Getenv("jobs") != "" {
			jobs := len(strings.Split(os.Getenv("jobs"), "\n"))
			if jobs < 1000 {
				elementToLoadString = " " + fmt.Sprint(jobs)
			} else {
				elementToLoadString = " " + fmt.Sprint(jobs/1000, "k")
			}
			elementToLoadString = " " + segmentStart(theme["jobInfo"]["color"]) + addFmt("inverted,"+theme["jobInfo"]["color"], elementToLoadString+" ") + segmentEnd(theme["jobInfo"]["color"])
		}
		break

	case "projectsInPath":
		//project in path
		if path, _ := os.Getwd(); path != os.Getenv("HOME") {
			filesToCheckFor := map[string]string{"Makefile": "Make", "Dockerfile": "Docker", "PKGBUILD": "PKGBUILD"}
			for file := range filesToCheckFor {
				if _, err := os.Stat(file); !os.IsNotExist(err) {
					elementToLoadString += " -> " + addFmt(theme["projectsInPath"]["sys_color"], filesToCheckFor[file])
				}
			}

			fileTypesToCheck := []string{"go", "py", "sh", "c", "cpp", "cs", "html", "js", "css", "nim", "rb"}
			isFileChecked := make(map[string]bool)
			dirTree, _ := ioutil.ReadDir("./")
			for _, file := range dirTree {
				for _, checkType := range fileTypesToCheck {
					if strings.Split(file.Name(), ".")[len(strings.Split(file.Name(), "."))-1] == checkType && !isFileChecked[checkType] {
						elementToLoadString += " -> " + addFmt(theme["projectsInPath"]["lang_color"], checkType)
						isFileChecked[checkType] = true
					}
				}
			}
		}
		break

	case "prompt":
		//prompt
		if os.Getenv("error") != "disabled" {
			promptChar := strings.Replace(promptChar, "\\n", "\n", -1)
			promptChar = strings.Replace(promptChar, "\\s", " ", -1)
			if os.Getenv("error") == "0" {
				elementToLoadString += addFmt("green", promptChar)
			} else {
				elementToLoadString += addFmt("red", promptChar)
			}
		}
		break

	case "segments":
		break

	default:
		if elementName == "" {
			return ""
		}
		var elementData = elementName
		if theme[elementName]["vars"] != "off" {
			elementData = strings.Replace(elementName, "\\n", "\n", -1)
			elementData = strings.Replace(elementData, "\\s", " ", -1)
			elementDataArr := strings.Split(elementName, " ")
			for n := range elementDataArr {
				if elementDataArr[n][0] == '$' {
					elementData = strings.Replace(elementName, elementDataArr[n], os.Getenv(elementDataArr[n][1:len(elementDataArr[n])]), -1)

				}
			}
		}
		switch theme[elementName]["type"] {
		case "powerline":
			if elementName != "" {
				elementToLoadString = " " + segmentStart(theme[elementName]["color"]) + addFmt("inverted,"+theme[elementName]["color"], " "+elementData+" ") + segmentEnd(theme[elementName]["color"])
			}
			break
		case "array":
			if elementName != "" {
				elementToLoadString = " " + segmentArray(strings.Split(theme[elementName]["color"], ","), strings.Split(elementData, ","))
			}
			break
		case "text":
			elementToLoadString = addFmt(theme[elementName]["color"], " "+elementData+" ")
			break
		case "txtnl":
			elementToLoadString = addFmt(theme[elementName]["color"], " "+elementData+" ") + "\n"
			break
		default:
			elementToLoadString = addFmt(theme[elementName]["color"], " "+elementData+" ")
			break
		}
		break
	}
	return elementToLoadString
}

func main() {
	promptChar = addFmt("bold", "-> ")

	if len(os.Args) < 2 { //if no arguments are passed or only a theme, start a shell
		var command string
		if len(os.Args) > 1 {
			command = "PROMPT_COMMAND='echo $(" + os.Args[0] + " status \"" + os.Args[1] + "' PS1=\"$(" + os.Args[0] + " prompt)\" sh"
		} else {
			command = "PROMPT_COMMAND='echo $(" + os.Args[0] + " status ' PS1=\"$(" + os.Args[0] + " prompt)\" sh"
		}
		cmd := exec.Command("sh", "-c", command)
		cmd.Stdin = os.Stdin
		cmd.Stderr = os.Stderr
		cmd.Stdout = os.Stdout
		if err := cmd.Run(); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		os.Exit(0)
	}

	if os.Args[1] == "status" {
		if os.Getenv("bearsh_theme") == "" {
			fmt.Println("Error: 'bearsh_theme' is undefined. Maybe try running 'bearsh getrice'")
			os.Exit(1)
		}

		themeRaw := strings.Split(strings.Replace(os.Getenv("bearsh_theme"), "\n", " ", -1), " ")
		theme = make(map[string]map[string]string)
		elementOrder := make([]string, len(themeRaw))
		for n, element := range themeRaw {
			data := strings.Split(element, ":")
			theme[data[0]] = make(map[string]string)
			if len(data) > 1 {
				for _, n2 := range data[1:len(data)] {
					data2 := strings.Split(n2, "=")
					if len(data2) > 1 {
						theme[data[0]][data2[0]] = data2[1]
					}
				}
			}
			elementOrder[n] = data[0]
		}
		promptChar = addFmt("bold", theme["prompt"]["char"]+" ")
		segmentType = theme["segments"]["type"]
		var statusLine string
		for _, nameOfElementToLoad := range elementOrder {
			statusLine += statusElement(nameOfElementToLoad)
		}
		fmt.Print(statusLine)
	}

	switch os.Args[1] {
	case "prompt":
		fmt.Print("\n$(if [ $? -eq 0 ]; then\necho \""+addFmt("green", promptChar)+"\"\nelse\necho \""+addFmt("red", promptChar)+"\"\nfi)", " ")
		break

	case "zsh":
		fmt.Println(`
export precmd() {
PS1=$(bearsh status "prompt:$?;theme:$bearsh_theme;jobs:$(jobs -r -p)")
}

# VVVV BearSH theme VVVV
bearsh_theme="
userInfo:root=bold,red
 pwd:locked_path_color=red:path_color=cyan
 gitInfo:branch_color=yellow:change_color=red
 projectsInPath:lang_color=green:sys_color=magenta
 jobInfo:color=yellow
 prompt:char=->
"
#^^^^ BearSH theme ^^^^`)
		break

	case "update":
		fmt.Println(addFmt("bold", "Checking for updates..."))
		var newestRelease string
		if newestReleaseHTTP, err := http.Get("https://gitlab.com/unic0rn9k/bearsh/raw/master/version"); err == nil {
			newestReleaseBytes, _ := ioutil.ReadAll(newestReleaseHTTP.Body)
			newestRelease = string(newestReleaseBytes)
		} else {
			fmt.Println(addFmt("red,bold", "Error: Unable to check for updates. please make sure you're connected to the internet!\n"), err)
			os.Exit(1)
		}
		if newestRelease[0] != 'v' {
			fmt.Println(addFmt("red,bold", "Error: Unable to check for updates. please make sure you're connected to the internet and maybe try a manual update!"))
			os.Exit(1)
		}
		fmt.Println(addFmt("bold", "Updating..."))
		if strings.Trim(newestRelease, "\n") == version {
			fmt.Println(addFmt("bold", "Bearsh is up to date!"))
			os.Exit(0)
		}
		if err := xCmd("git clone https://gitlab.com/unic0rn9k/bearsh.git ~/bearsh"); err != nil {
			fmt.Println(addFmt("red,bold", "Error: Unable to clone git repo. maybe try running 'rm -rf ~/bearsh'"))
			os.Exit(1)
		}
		if err := xCmd("make -C ~/bearsh update"); err != nil {
			fmt.Println(addFmt("red,bold", "Error: unable to install bearsh!"))
			os.Exit(1)
		}
		fmt.Println(addFmt("bold", "Cleaning build dir..."))
		if err := xCmd("rm -rf ~/bearsh"); err != nil {
			fmt.Println(addFmt("bold,yellow", "WARNING:") + " Unable to clean bearsh build dir!")
		}
		fmt.Println(addFmt("bold", "Bearsh successfully updated!"))
		break

	case "updateAuto":
		fmt.Println(addFmt("bold", "Checking for updates..."))
		var newestRelease string
		if newestReleaseHTTP, err := http.Get("https://gitlab.com/unic0rn9k/bearsh/raw/master/version"); err == nil {
			newestReleaseBytes, _ := ioutil.ReadAll(newestReleaseHTTP.Body)
			newestRelease = string(newestReleaseBytes)
		} else {
			fmt.Println(addFmt("red,bold", "Error: Unable to check for updates. please make sure you're connected to the internet!\n"), err)
			os.Exit(1)
		}
		if newestRelease[0] != 'v' {
			fmt.Println(addFmt("red,bold", "Error: Unable to check for updates. please make sure you're connected to the internet and maybe try a manual update!"))
			os.Exit(1)
		}
		fmt.Println(addFmt("bold", "Updating..."))
		if strings.Trim(newestRelease, "\n") == version {
			fmt.Println(addFmt("bold", "Bearsh is up to date!"))
			os.Exit(0)
		}
		if err := xCmd("git clone https://gitlab.com/unic0rn9k/bearsh.git ~/bearsh"); err != nil {
			fmt.Println(addFmt("red,bold", "Error: Unable to clone git repo. maybe try running 'rm -rf ~/bearsh'"))
			os.Exit(1)
		}
		if err := xCmd("make -C ~/bearsh default"); err != nil {
			fmt.Println(addFmt("red,bold", "Error: unable to install bearsh!"))
			os.Exit(1)
		}
		fmt.Println(addFmt("bold", "Cleaning build dir..."))
		if err := xCmd("rm -rf ~/bearsh"); err != nil {
			fmt.Println(addFmt("bold,yellow", "WARNING:") + " Unable to clean bearsh build dir!")
		}
		fmt.Println(addFmt("bold", "Bearsh successfully updated!"))
		break

	case "uninstall":
		fmt.Println("bye bye...")
		if err := xCmd("sudo rm -rf ~/bearsh ~/.bearsh /bin/bearsh"); err != nil {
			fmt.Println(addFmt("bold,red", "Error: Unable to uninstall bearsh!"))
		}
		fmt.Println(addFmt("bold", "NOTE:"), "your zshrc file might be in '~/.zshrc.old' or '~/.zshrc'")
		break

	case "getrice":
		fmt.Println("fetching repositories...")
		var repos string
		if _, err := os.Stat("~/.bearsh/rice-repo"); os.IsNotExist(err) {
			if newestReleaseHTTP, err := http.Get("https://gitlab.com/unic0rn9k/bearsh/raw/master/rice-repo"); err == nil {
				newestReleaseBytes, _ := ioutil.ReadAll(newestReleaseHTTP.Body)
				repos = string(newestReleaseBytes)
			} else {
				fmt.Println(addFmt("red,bold", "Error: Unable to fetch repositories. please make sure you're connected to the internet!\n"), err)
				os.Exit(1)
			}
		} else {
			var reposToCheck string
			if data, err := ioutil.ReadFile("~/.bearsh/rice-repo"); err == nil {
				reposToCheck = string(data)
			} else {
				fmt.Println(addFmt("red,bold", "Error: Unable to open repo file."), err)
			}
			for _, repo := range strings.Split(reposToCheck, "\n") {
				fmt.Print("Fetching ", repo, "... ")
				if newestReleaseHTTP, err := http.Get(repo); err == nil {
					newestReleaseBytes, _ := ioutil.ReadAll(newestReleaseHTTP.Body)
					repos += string(newestReleaseBytes)
					fmt.Println("OK")
				} else {
					fmt.Println(addFmt("red,bold", "Error: Unable to fetch repositories. please make sure you're connected to the internet and your ~/.bearsh/rice-repo is valid!\n"), err)
				}
			}
		}
		fmt.Println("available rice:")
		var riceList = make(map[string]string)
		for _, rice := range strings.Split(repos, "\n") {
			data := strings.Split(rice, " : ")
			if len(data) > 1 {
				riceList[data[0]] = data[1]
				fmt.Println(" ", data[0])
			}
		}
		if len(riceList) == 0 {
			fmt.Println(addFmt("yellow,bold", "Warning: "), "Repositories are empty!")
			os.Exit(0)
		}
		fmt.Println(addFmt("bold", "NOTE:"), "'~/.zshrc' will be moved to '~/.zshrc.old'")
		fmt.Print("which rice do you want to install? ")

		selectedRice, _ := bufio.NewReader(os.Stdin).ReadString('\n')
		selectedRice = strings.Trim(selectedRice, "\n")
		if err := xCmd("cp -f ~/.zshrc ~/.zshrc.old"); err != nil {
			fmt.Println(addFmt("bold,yellow", "WARNING:") + " Unable to move ~/.zshrc!")
		}
		out, err := os.Create(os.Getenv("HOME") + "/.zshrc")
		if err != nil {
			fmt.Println(addFmt("red,bold", "Error: unable to open ~/.zshrc"), err)
			os.Exit(1)
		}
		defer out.Close()
		fmt.Print("Fetching ", riceList[selectedRice], "...\n")
		riceData, err := http.Get(riceList[selectedRice])
		if err != nil {
			fmt.Println(addFmt("red,bold", "Error: unable to fetch rice!"), err)
			os.Exit(1)
		}
		defer riceData.Body.Close()
		_, err = io.Copy(out, riceData.Body)
		if err != nil {
			fmt.Println(addFmt("red,bold", "Error: unable to write rice!"), err)
			os.Exit(1)
		}
		riceData.Body.Close()
		out.Close()
		zshrc, err := os.Open(os.Getenv("HOME") + "/.zshrc")
		if err != nil {
			fmt.Println(addFmt("red,bold", "Error: Unable to open ~/.zshrc!"), err)
		}
		defer zshrc.Close()
		scanner := bufio.NewScanner(zshrc)
		for scanner.Scan() {
			data := strings.Split(scanner.Text(), " : ")
			if len(data) > 2 && data[0] == "#bearsh git" {
				if _, err := os.Stat(os.Getenv("HOME") + "/.bearsh/" + data[2]); os.IsNotExist(err) {
					fmt.Print("Fetching dependency ", data[2], "... ")
					if _, err := sxCmd("git clone " + data[1] + " ~/.bearsh/" + data[2]); err != nil {
						fmt.Println(addFmt("red,bold", "Error: Unable to fetch ", data[2], "!"), err)
					} else {
						fmt.Println("OK")
					}
				}
			}
			if len(data) > 1 && data[0] == "NOTE" {
				fmt.Println(addFmt("bold", data[1]))
			}
		}
		fmt.Println(addFmt("bold", "Rice successfully installed!"))
		break
	}
}
