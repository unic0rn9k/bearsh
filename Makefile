build: main.go
	go build -o bearsh main.go
clean:
	@rm -rfv ./bearsh
remove:
	sudo rm -rfv ./bearsh /bin/bearsh
install: ./default.zshrc
	@echo " 1: default (recommended)"
	@echo " 2: simple"
	@echo " 3: oh-my-zsh"
	@echo " 4: update (this option might break bearsh!)"
	@echo " 5: just install bearsh (bin)"
	@echo "other themes/rice can be installed after you install bearsh by running 'bearsh getrice'"

	-@read -p "`tput bold`Choose a bearsh version: `tput sgr0`" installv; \
	if [[ "x$$installv" == 'x5' ]]; \
	then \
		make build; \
		sudo cp -rfv ./bearsh /bin/bearsh; \
		make clean; \
	elif [[ "x$$installv" == 'x2' ]]; \
	then \
		make update; \
		echo "`tput bold`NOTE:`tput sgr0` $$HOME/.zshrc will be moved to $$HOME/.zshrc.old"; \
		mv -fv $$HOME/.zshrc $$HOME/.zshrc.old; \
		cp -rfv themes/simple.zshrc $$HOME/.zshrc; \
	elif [[ "x$$installv" == 'x3' ]]; \
	then \
		make install_oh-my-zsh; \
	elif [[ "x$$installv" == 'x4' ]]; \
	then \
		make update; \
	else \
		make update; \
		echo "`tput bold`NOTE:`tput sgr0` $$HOME/.zshrc will be moved to $$HOME/.zshrc.old"; \
		mv -fv $$HOME/.zshrc $$HOME/.zshrc.old; \
		cp -rfv default.zshrc $$HOME/.zshrc; \
	fi
	
	@echo "`tput bold`bearsh successfully installed!`tput sgr0`"

install_simple: ./themes/simple.zshrc
	@make build
	sudo cp -rfv ./bearsh /bin/bearsh
	@make clean
	
	-@read -p "`tput bold`Do you want to install bearsh as a wrapper for zsh? [Y/n]`tput sgr0`" installzsh; \
	if [[ "x$$installzsh" != 'xn' ]] && [[ "x$$installzsh" != 'xN' ]]; \
	then \
		echo "installing bearsh for zsh..."; \
		sh -c "$$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh) --unattended"; \
		echo "installing zsh plugins..."; \
		git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting; \
		git clone https://github.com/zsh-users/zsh-autosuggestions $${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions; \
		git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf; \
		~/.fzf/install --no-update-rc; \
		mkdir -v ~/.bearsh ~/.bearsh/cache ~/.bearsh/notes; \
		cp -rfv bear.zsh ~/.bearsh; \
		echo "`tput bold`NOTE:`tput sgr0` $$HOME/.zshrc will be moved to $$HOME/.zshrc.old"; \
		mv -fv $$HOME/.zshrc $$HOME/.zshrc.old; \
		cp -rfv themes/simple.zshrc $$HOME/.zshrc; \
	fi
	
	@echo "`tput bold`bearsh successfully installed!`tput sgr0`"

update:
	@make build
	sudo cp -rfv ./bearsh /bin/bearsh
	@make clean
	
	-@read -p "`tput bold`Do you want to install bearsh as a wrapper for zsh? [Y/n]`tput sgr0`" installzsh; \
	if [[ "x$$installzsh" != 'xn' ]] && [[ "x$$installzsh" != 'xN' ]]; \
	then \
		echo "installing bearsh for zsh..."; \
		mkdir -v ~/.bearsh ~/.bearsh/cache ~/.bearsh/notes; \
		cp -rfv bear.zsh ~/.bearsh; \
		echo "installing zsh plugins..."; \
		git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $$HOME/.bearsh/zsh-syntax-highlighting; \
		git clone https://github.com/zsh-users/zsh-autosuggestions $$HOME/.bearsh/zsh-autosuggestions; \
		git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf; \
		git clone https://github.com/popstas/zsh-command-time.git ~/.bearsh/zsh-command-time; \
		~/.fzf/install --no-update-rc; \
	fi

default:
	@make build
	sudo cp -rfv ./bearsh /bin/bearsh
	@make clean
	
	echo "installing bearsh for zsh..."; \
	mkdir -v ~/.bearsh ~/.bearsh/cache ~/.bearsh/notes; \
	cp -rfv bear.zsh ~/.bearsh; \
	echo "installing zsh plugins..."; \
	git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $$HOME/.bearsh/zsh-syntax-highlighting; \
	git clone https://github.com/zsh-users/zsh-autosuggestions $$HOME/.bearsh/zsh-autosuggestions; \
	git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf; \
	git clone https://github.com/popstas/zsh-command-time.git ~/.bearsh/zsh-command-time; \
	~/.fzf/install --no-update-rc
	echo "`tput bold`NOTE:`tput sgr0` $$HOME/.zshrc will be moved to $$HOME/.zshrc.old"; \
	mv -fv $$HOME/.zshrc $$HOME/.zshrc.old; \
	cp -rfv default.zshrc $$HOME/.zshrc; \

install_oh-my-zsh: ./themes/oh-my-zsh.zshrc
	@make build
	sudo cp -rfv ./bearsh /bin/bearsh
	@make clean
	
	@echo "installing bearsh for zsh..."
	-@sh -c "$$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh) --unattended"
	@echo "installing zsh plugins..."
	-git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
	-git clone https://github.com/zsh-users/zsh-autosuggestions $${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
	-git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
	-~/.fzf/install --no-update-rc
	@echo "`tput bold`NOTE:`tput sgr0` $$HOME/.zshrc will be moved to $$HOME/.zshrc.old"
	-mv -fv $$HOME/.zshrc $$HOME/.zshrc.old
	cp -rfv themes/oh-my-zsh.zshrc $$HOME/.zshrc

	@echo "`tput bold`bearsh successfully installed!`tput sgr0`"
