# this file came from bearsh. the old ".zshrc" file can be found in "~/.zshrc.old". to add bearsh to an existing zsh config, run: bearsh zsh >> .zshrc
export ZSH="$HOME/.bearsh"

plugins=(
	zsh-syntax-highlighting
	zsh-autosuggestions
	zsh-command-time
)

# VVVV BearSH theme VVVV
export bearsh_theme="
userInfo:root=bold,red
 pwd:locked_path_color=red,bold:path_color=cyan
 gitInfo:branch_color=dark_gray:change_color=red:branch_icon_color=dark_gray:branch_change_color=yellow
 jobInfo:color=yellow
 prompt:char=\n->
 segments:type=powerline
"
# ^^^^ BearSH theme ^^^^

export EDITOR='nvim' # <- remember to set your text editor ;)

source $ZSH/bear.zsh

todo ls